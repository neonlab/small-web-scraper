
//const express = require('express')
//const path = require('path')
/*
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const PORT = process.env.PORT || 5000
*/
const { parse } = require('querystring');

var express = require('express');
var app = express();
var Crawler = require("js-crawler");
var request = require("request");
var cheerio = require("cheerio");

var saveFolder = "autos";

var startAt = 420;
var endAt = 560;

/* Folder para guardar los datos */
var fs = require('fs');
var dir = './'+saveFolder+'/';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

Start(); /// STARTS THE PROGRAM.

function Start()
{
    for(var i = startAt ; i < endAt; i++)
    {
      GetRequest(i)

    }
}



function GetRequest(index)
{

  const https = require("http");
  const url = "http://www.alejandroautos.com.ar/detalle.php?id="+index;
  https.get(url, res => {
    res.setEncoding("utf8");
    let body = "";
    res.on("data", data => {
      body += data;
    });
    res.on("end", () => {
    
       FindAutos(body,index);
      //console.log(body);
    });
  });

}




/*The following example shows how cheerio is used to parse HTML strings. 
The first line imports cheerio into the program. The html variable holds 
the HTML fragment to be parsed. On line 3, the HTML is parsed using cheerio. 
The result is assigned to the $ variable. The dollar sign was chosen because
it is traditionally used in jQuery. Line 4 selects the <ul> element using CSS style selectors.
Finally, the list’s inner HTML is printed using the html() method.
*/




async function FindAutos(bod,i)
{  

  console.log("=============="+i);
    var currentAt = i;
    var html = bod;
    var $ = cheerio.load(html);
    var list = $("tbody");
    //console.log(list.html());
    var stringToFilter = list.html();
    //console.log(stringToFilter);
    var nombre = stringToFilter.split('<td height="93" colspan="5"><div align="left"><span class="txt_rojo_40">').pop().split('</span> </div></td>');
    var detalles = stringToFilter.split('<td colspan="5"><div align="left"><span class="txt_gris_35">').pop().split('</span></div></td>');
    var detalles2 = stringToFilter.split('<td colspan="5"><div align="left"><span class="txt_gris_25">').pop().split('</span></div></td>');
    var detalles3 = stringToFilter.split('<td colspan="5"><div align="left"><span class="txt_gris_12">').pop().split('</span></div></td>');
    
    var fNombre = removeAllWords(nombre[0],'&#xA0;');
    
    fNombre = removeAllWords(fNombre,'<br>');
   // fNombre = fNombre.replace(/\s/g, '-'); //Remueve todos los espacios en blanco.
    fNombre = fNombre.trim(); //Remueve el espacio del final en blanco.
    fNombre = removeAllLineBreaks(fNombre);
    createDir(fNombre);

    detalles = removeAllWords(detalles[0],'&#xA0;');
    detalles += " ";
    detalles += removeAllWords(detalles2[0],'&#xA0;');
    detalles += " ";
    detalles += removeAllWords(detalles3[0],'&#xA0;');
    //console.log (detalles);
    createTextFiles(fNombre,detalles);
    
    //http://www.alejandroautos.com.ar/autos/455_1.jpg
    
    for(var j = 0 ; j < 4 ; j++)
    {
      SaveImage("http://www.alejandroautos.com.ar/autos/",currentAt+"_"+j+".jpg",saveFolder+"/"+fNombre);
    }
    
    // console.log(list.html());//console.log(list.html());
}

function removeAllWords(stringToRemoveWords,word)
{
    while(stringToRemoveWords.match(word))
    {
      stringToRemoveWords = stringToRemoveWords.replace(word, '');
    }
    return stringToRemoveWords;
}

function createTextFiles(folder,content)
{
  var fs = require("fs");

  var data = content;

  fs.writeFile(saveFolder+"/"+folder+"/datos.txt", data, (err) => {
    if (err) console.log(err);
    console.log("Successfully Written to File.");
  });
}

function removeAllLineBreaks(stringToRemoveLineBreaks)
{
  var reg = /\r?\n|\r/;

  while(stringToRemoveLineBreaks.match(reg))
  {
    stringToRemoveLineBreaks = stringToRemoveLineBreaks.replace(reg, '');
  }
  return stringToRemoveLineBreaks;
}

function createDir(nombreDelDirectorio)
{

  var fs = require('fs');
  var dir = './'+saveFolder+'/'+nombreDelDirectorio;
  
  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
}



function SaveImage(URL,imgName,pathToSave)
{

  
  console.log("???" + URL+" - "+imgName+ " - "+ pathToSave);
  const download = require('image-downloader')
 
// Download to a directory and save with an another filename
options = {
  url: URL+imgName, //http://someurl.com/image.jpg'
  dest: pathToSave        // path/to/dest/photo.jpg
}
 
download.image(options)
  .then(({ filename, image }) => {
    console.log('File saved to', filename)
  })
  .catch((err) => {
    console.error(err)
  })
  
}


function Auto(modelo,año,detalle) {
    this.modelo = make,
    this.año = model,
    this.detalle = color,
    this.fotos = [];
}


/*
var crawler = new Crawler().configure({ignoreRelative: false, depth: 0});
 
crawler.crawl({
  url: "http://www.alejandroautos.com.ar/detalle.php?id=455",
  success: function(page) {
    console.log(page.url);
        console.log(page.content);

  },
  failure: function(page) {
    console.log(page.status);
  },
  finished: function(crawledUrls) {
    //console.log(crawledUrls);
  }
});

/*

var server = require('http').createServer((request,response)=>{
    if (request.method === 'POST') 
    {
        let body = [];
        request.on('data', (chunk) => 
        {
          body.push(chunk);
        }).on('end', () => 
        {
            
          var chunk = ParseToObject(request.url,body);
         // var stringeChunk = JSON.stringify(chunk);
          let buf = Buffer.from(JSON.stringify(chunk));
          body.push(buf);
          body = Buffer.concat(body).toString();
          response.end(body);
        });
      } else 
      {
        response.statusCode = 404;
        response.end();
      }
    });
    
server.listen(porty, () => {
    console.log("\x1b[34m",'Server listening at port %d', porty);
  });
  */

