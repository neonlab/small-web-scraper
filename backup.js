
//const express = require('express')
//const path = require('path')
/*
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const PORT = process.env.PORT || 5000
*/

var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'public')));


var pool = 100;
var playerCount=0;
var clients=[];
var testuniquecode = 123;

io.on('connection',function(socket)
{
    var CurrentUser={};
    CurrentUser.UniqueID='';
    CurrentUser.Balance='';
    playerCount++;
    console.log("Cantidad de jugadores:"+ playerCount);

    /*
    if(!CurrentUser.UniqueID)
    {
    socket.emit('OnConnect');
    }
    */
    
    socket.on('OnLogin',function(obj)
    {   
        if(obj.uniqueID==testuniquecode)
        {
            console.log("Nuevo usuario");
            var user=   
            {
                Name:obj.username,
                UniqueID:obj.uniqueID,
                Balance:100 /* La variabl obj no deberia traer el balance, deberia provenir dl sv.*/
            };
            
            CurrentUser = user;
            console.log("Usuario logado, Username: "+ CurrentUser.Name + " UniqueID:" +CurrentUser.UniqueID+ " Balance:"+ CurrentUser.Balance);
            socket.emit('OnLoginAuthenticated',{status:true});
         }   
         else
         {
            socket.emit('OnLoginAuthenticated',{status:false});
             console.log("Fail Login");
         }
        //socket.emit('OnUpdateBalance',{number:CurrentUser.Balance});
    });
    /* Este socket es el encargado de recibir las apuestas
     * Cada elemento obj es es tratado como el objeto origianl.
     * Por ejemplo en la clase original de c# ("BetJSON.cs")
     * tenía un array que se llamaba "BetCollection".
     * Podemos acceder a el simplemente obj.BetCollection.
     */
    socket.on('BET',function(obj)
    {
        console.log("Nuevo MSJ: " + obj.BetCollection);
        var arSize = obj.BetCollection.length;
        var selectedIndex = Calculate(obj.BetCollection);
        console.log("El balance del usuario es: "+ CurrentUser.Balance);
        CalculateUserBalance(selectedIndex,obj.BetCollection);
        console.log("El balance del usuario ahora es: "+ CurrentUser.Balance);

        console.log(selectedIndex);
        socket.emit('betResult',{number:selectedIndex});
    });

    socket.on('GETBALANCE',function(obj)
    {
        console.log("El balance del usuario es: " + CurrentUser.Balance);
        socket.emit('OnUpdateBalance',{number:CurrentUser.Balance});
    });

    socket.on('SETBALANCE',function(obj)
    {
        console.log("seteando balance");
        CurrentUser.Balance = obj.number;
    });

    socket.on('disconnect',function()
    {
       playerCount--; 
       console.log("Cantidad de jugadores:"+ playerCount);
    });

    function CalculateUserBalance(winnerNumber,betedNumbers=[])
{
    var valueToRest=0;
    for(var i = 0; i < betedNumbers.length ; i++)
    {
        if(i!=winnerNumber)
        valueToRest+=betedNumbers[i];
    }
    CurrentUser.Balance-=valueToRest;
    CurrentUser.Balance+=betedNumbers[winnerNumber];
}

});//End of socket

function Calculate(betAr=[])
{
    var randomNumbers = CreateARandomNumberArray(1000);
   for(var i = 0; i < randomNumbers.length ; i++)
   {
       if(betAr[randomNumbers[i]] < pool/2)
       return randomNumbers[i];
   }
   return 0;
}



function CreateARandomNumberArray(amountOfNumbersToGenerate)
{
    var randomNumberCollection = [];
    while(amountOfNumbersToGenerate!=0)
    {
        var x = GenerateRandomNumber();
        randomNumberCollection.push(x);
        amountOfNumbersToGenerate--;
    }
    return randomNumberCollection;
};




function GenerateRandomNumber ()
{
  return Math.floor(Math.random() * (-1 - 37 + 1) ) + 37;
};


server.listen(port, () => {
    console.log('Server listening at port %d', port);
  });

/*
http.listen(process.env.PORT, function()
{
    console.log("El server esta corriendo en el puerto: "+PORT);
});

/*
var http = require('http');
http.createServer(function (req, res) {
   res.writeHead(200, {'Content-Type': 'text/plain'});
   res.end('Hello World\n');
}).listen(process.env.PORT);
console.log('Server running at http://127.0.0.1:'+ PORT);
*/

